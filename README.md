# VoterPeer Web Portal

Censorship resistant on-chain blockchain voting.

**[See Documentation and project details](https://docs.voter.cash)**

This repository is for the web portal for creating votes, viewing votes and managing participant lists.

## User interface

The folder `public` contains the user interface. It is built and deployed as a static website. In our deployment, we use Gitlab pages and deploy it as part of CI/CD on merges to the master branch.

The website uses the [Vue.js](https://vuejs.org/) framework and [Bootstrap](https://bootstrap-vue.org/) front-end library.

## Backend API

The folder `functions` contains the backend REST API. The API is deployed as [Firebase Cloud Functions](https://firebase.google.com/docs/functions), but written to be compatible with [Express](https://expressjs.com/) web framework for portability with other deployment options.

The backend API is used by the frontend, the android app and integration on other websites, including the Bitcoin Unlimited website.

## Deployment configuration

### One-time setup

- Install Node.js
- Set up a firebase project

Log into firebase

`(cd functions; npm run login)`

### Configuration files

|File | Description |
|-----|-------------|
| `voterpeer.conf` | Firebase hosting configuration |
| `firebase.json` | Firebase hosting configuration |

## Developers

To run the tests on a firebase emulator (`npm run emulate`) you need Java installed.

### For Mac users

### Build

To build both node server and vue application, run `npm run build`. These can
also be built separately by running `npm run build` in the `functions` or
`public` folder.

Some of the npm dependencies may require xcode build tools.

`xcode-select --install`

### Running

#### Run front-end locally

`(cd public; npm run serve)`

#### Run functions locally

[Official docs](https://firebase.google.com/docs/functions/local-emulator)

Set up admin credentials for emulated functions:
1. Open the [Service Accounts pane](https://console.cloud.google.com/iam-admin/serviceaccounts)
of the Google Cloud Console.
2Make sure that App Engine default service account is selected, and use the
options menu at right to select Create key.
3. When prompted, select JSON for the key type, and click Create.

Set your Google default credentials to point to the downloaded key. Use the
path functions/keys to store the key, and make sure that it is in gitignore:

On Unix:
```bash
export GOOGLE_APPLICATION_CREDENTIALS="<absolute_path>/functions/keys/<KeyFileName>.json"
npm run emulate
```

On Windows:
```shell
set GOOGLE_APPLICATION_CREDENTIALS=functions\keys\<KeyFileName>.json
npm run emulate
```

### Testing functions

```bash
npm run test
```

### Logs from all functions

`firebase functions:log`

### Pitfalls

Verify that:
`firebase-adminsdk-aboe1@<PROJECTID>.iam.gserviceaccount.com` and `<PROJECTID>@appspot.gserviceaccount.com`
has "Service Account Token Creator" permissions in google cloud dashboard
located
here: `https://console.cloud.google.com/iam-admin/iam?project=<PROJECTID>`


