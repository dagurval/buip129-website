import { Blockchain } from '@bitcoinunlimited/votepeerjs';

import { CashAddressType, decodeCashAddress } from '@bitauth/libauth';
import firebase from 'firebase/app';
import admin from 'firebase-admin';
import crypto from 'crypto';
import assert from 'assert';
import { getTwoOptionVoteTally, getRingSignatureVoteTally, getMultiOptionVoteTally } from './tally';
import { getElection, listElections, QueryCallback } from './election_database';
import {
  hasRingSignaturePubKey,
  getRingSignaturePubkey,
  getUserAllowedTags,
} from '../user/user_database';
import {
  Election,
  CONTRACT_TYPE_MULTI_OPTION_VOTE,
  CONTRACT_TYPE_TWO_OPTION_VOTE,
  CONTRACT_TYPE_RING_SIGNATURE,
  VISIBILITY_PARTICIPANTS,
  VISIBILITY_PUBLIC,
} from './model/election';
import {
  getGetParam,
  getGetParamOr,
  getPostParam,
  isLowerCaseBitcoinCashAddress,
  getPostParamOr,
} from '../util';
import {
  ServerError,
  InvalidParamError,
  AuthenticationError,
  NoAccessError,
} from '../error';
import { logger, firebaseAdminFirestore } from '../db';
import { getAuthenticatedUser } from '../rpcauth';
import { constants } from '../constants';

// If you change this constant from 100, remember to also update the
// backend API documentation.
export const DEFAULT_LIST_ELECTIONS_LIMIT = 100;

export async function getTally(req: any, res: any,
  dummyuser: boolean = false): Promise<string> {
  let user = null;
  try {
    user = await getAuthenticatedUser(req, dummyuser);
  } catch (e) {
    if (e instanceof AuthenticationError) {
      // Unauthenticated is OK for elections with public visibility.
      // Check for public visiblity is below.
    } else {
      throw e;
    }
  }
  const electionID = getGetParam<string>(req, 'election_id');
  const election = await getElection(electionID);

  // User must be part of this election to access this information.
  if (election.visibility !== VISIBILITY_PUBLIC) {
    if (user === null) {
      throw new InvalidParamError('User must be authenticated to tally this election.');
    }

    if (user.user_id !== election.adminAddress
        && !election.participantAddresses.includes(user.user_id)) {
      throw new InvalidParamError(`User ${user.user_id} is not part of election`);
    }
  }

  const electrum = new Blockchain();
  await electrum.connect();
  try {
    if (election.contractType === CONTRACT_TYPE_TWO_OPTION_VOTE) {
      return JSON.stringify(await getTwoOptionVoteTally(electrum, election));
    }
    if (election.contractType === CONTRACT_TYPE_RING_SIGNATURE) {
      return JSON.stringify(await getRingSignatureVoteTally(electrum, election));
    }
    if (election.contractType === CONTRACT_TYPE_MULTI_OPTION_VOTE) {
      return JSON.stringify(await getMultiOptionVoteTally(electrum, election));
    }
    throw new InvalidParamError(
      'election_id',
      'API does not support tallying for this election contract type.',
    );
  } finally {
    electrum.disconnect();
  }
}

const NETWORK_PREFIX = 'bitcoincash:';

async function throwUnlessValidParticipants(contractType: string, participants: string[]) {
  const arg = "'participants'";
  if (participants.length === 0) {
    throw new InvalidParamError(arg, 'Empty');
  }
  for (const p of participants) {
    if (!p.startsWith(NETWORK_PREFIX)) {
      throw new InvalidParamError(arg,
        `Participants must start with ${NETWORK_PREFIX} (failed for '${p}')`);
    }
    if (!isLowerCaseBitcoinCashAddress(p)) {
      throw new InvalidParamError(arg,
        `Participants must be lower case addresses (failed for '${p}')`);
    }
    const decoded: any = decodeCashAddress(p);
    if (decoded.type !== CashAddressType.P2PKH) {
      throw new InvalidParamError(arg,
        `Participants must be P2PKH addresses (failed for '${p}')`);
    }
    if (contractType === CONTRACT_TYPE_RING_SIGNATURE) {
      if (!await hasRingSignaturePubKey(p)) {
        throw new InvalidParamError('participants', `Participant '${p}' has not enabled `
                                           + 'anonymous voting. They need to open '
                                           + 'their mobile app after upgrading to '
                                           + 'a recent version');
      }
    }
  }
  if ((new Set(participants)).size !== participants.length) {
    throw new InvalidParamError(arg, 'Duplicate values');
  }
}

async function createTwoOptionVote(
  electionCollection: any,
  owner: string,
  salt: string,
  createdAt: firebase.firestore.Timestamp,
  modifiedAt: firebase.firestore.Timestamp,
  description: string,
  options: string[],
  participants: string[],
  endHeight: number,
  visibility: string,
  tags: string[],
) {
  const doc = await electionCollection.add({
    adminAddress: owner,
    contractType: CONTRACT_TYPE_TWO_OPTION_VOTE,
    createdAt,
    description,
    endHeight,
    modifiedAt,
    optionA: options[0],
    optionB: options[1],
    participantAddresses: participants,
    salt,
    visibility,
    tags,
  });
  logger().info(`Created ${CONTRACT_TYPE_TWO_OPTION_VOTE} election: ${doc.id}`);
  return doc.id;
}

async function createRingSignatureVote(
  electionCollection: any,
  user: string,
  salt: string,
  createdAt: firebase.firestore.Timestamp,
  modifiedAt: firebase.firestore.Timestamp,
  description: string,
  options: string[],
  participants: string[],
  beginHeight: number,
  endHeight: number,
  visibility: string,
  tags: string[],
) {
  const doc = await electionCollection.add({
    adminAddress: user,
    beginHeight,
    contractType: CONTRACT_TYPE_RING_SIGNATURE,
    createdAt,
    description,
    endHeight,
    modifiedAt,
    option: options,
    participantAddresses: participants,
    salt,
    visibility,
    tags,
  });
  logger().info(`Created ${CONTRACT_TYPE_RING_SIGNATURE} election: ${doc.id}`);
  return doc.id;
}

async function createMultiOptionVote(
  electionCollection: any,
  user: string,
  salt: string,
  createdAt: firebase.firestore.Timestamp,
  modifiedAt: firebase.firestore.Timestamp,
  description: string,
  options: string[],
  participants: string[],
  beginHeight: number,
  endHeight: number,
  visibility: string,
  tags: string[],
) {
  const doc = await electionCollection.add({
    adminAddress: user,
    beginHeight,
    contractType: CONTRACT_TYPE_MULTI_OPTION_VOTE,
    createdAt,
    description,
    endHeight,
    modifiedAt,
    option: options,
    participantAddresses: participants,
    salt,
    visibility,
    tags,
  });
  logger().info(`Created ${CONTRACT_TYPE_MULTI_OPTION_VOTE} election: ${doc.id}`);
  return doc.id;
}

/**
 * Check if user is allowed to assign `tags` to an election.
 */
async function throwUnlessCanUseTags(user: string, tags: string[]) {
  if (tags.length === 0) {
    return;
  }
  const allowedTags = await getUserAllowedTags(user);
  for (const t of tags) {
    if (!allowedTags.includes(t)) {
      throw new InvalidParamError('tags', `User is not allowed to use tag '${t}'`);
    }
  }
}

/**
 * API call for creating a new election.
 */
export async function createElection(req: any, res: any,
  dummyuser: boolean = false): Promise<string> {
  const user = await getAuthenticatedUser(req, dummyuser);
  const salt = crypto.randomBytes(32).toString('hex');
  const createdAt = admin.firestore.Timestamp.now();
  const modifiedAt = admin.firestore.Timestamp.now();

  const options = getPostParam<string[]>(req, 'options');
  const participants = getPostParam<string[]>(req, 'participants');
  const description = getPostParam<string>(req, 'description');
  const contractType = getPostParam<string>(req, 'contract_type');
  const beginHeight = contractType !== CONTRACT_TYPE_TWO_OPTION_VOTE
    ? getPostParam<number>(req, 'begin_height')
    : -1;

  const endHeight = getPostParam<number>(req, 'end_height');
  const visibility = getPostParam<string>(req, 'visibility');
  const tags = getPostParamOr<string[]>(req, 'tags', []);

  if (visibility !== VISIBILITY_PARTICIPANTS
      && visibility !== VISIBILITY_PUBLIC) {
    throw new InvalidParamError(
      'visibility',
      `Must be '${VISIBILITY_PARTICIPANTS}' or '${VISIBILITY_PUBLIC}'`,
    );
  }

  await Promise.all([
    throwUnlessCanUseTags(user.user_id, tags),
    throwUnlessValidParticipants(contractType, participants),
  ]);

  const electionCollection = await firebaseAdminFirestore().collection('election');
  const owner = user.user_id;
  if (!isLowerCaseBitcoinCashAddress(owner)) {
    throw new ServerError(`Invalid user ID for election admin '${owner}'`);
  }

  let electionID: string = '';
  if (contractType === CONTRACT_TYPE_TWO_OPTION_VOTE) {
    electionID = await createTwoOptionVote(
      electionCollection, owner, salt,
      createdAt, modifiedAt,
      description, options,
      participants, endHeight, visibility, tags,
    );
  } else if (contractType === CONTRACT_TYPE_RING_SIGNATURE) {
    electionID = await createRingSignatureVote(
      electionCollection, owner, salt,
      createdAt, modifiedAt, description, options,
      participants, beginHeight, endHeight, visibility, tags,
    );
  } else if (contractType === CONTRACT_TYPE_MULTI_OPTION_VOTE) {
    electionID = await createMultiOptionVote(
      electionCollection, owner, salt,
      createdAt, modifiedAt, description, options,
      participants, beginHeight, endHeight, visibility, tags,
    );
  } else {
    throw new InvalidParamError('contract_type',
      `Supported values: ${CONTRACT_TYPE_TWO_OPTION_VOTE}, ${CONTRACT_TYPE_RING_SIGNATURE}, ${CONTRACT_TYPE_MULTI_OPTION_VOTE}`);
  }
  assert(electionID !== '');

  return JSON.stringify({
    status: 'Election created',
    election_id: electionID,
  });
}

/**
 * Get linked public keys used in an election.
 */
export async function getLinkedKeys(
  req: any, res: any, dummyuser: boolean = false,
): Promise<string> {
  const user = await getAuthenticatedUser(req, dummyuser);
  const electionID = getGetParam<string>(req, 'election_id');
  const election = await getElection(electionID);

  // Election must require linked pubkeys
  if (election.contractType !== CONTRACT_TYPE_RING_SIGNATURE) {
    throw new InvalidParamError('Election does not require linked keys');
  }

  // User must be part of this election to access this information.
  if (user.user_id !== election.adminAddress
        && !election.participantAddresses.includes(user.user_id)) {
    throw new InvalidParamError(`User ${user.user_id} is not part of election`);
  }

  const keysPromises = election.participantAddresses.map((p) => getRingSignaturePubkey(p));
  const keys = await Promise.all(keysPromises);

  const result: Map<string, string> = new Map();

  election.participantAddresses.forEach((p, i) => {
    result.set(p, keys[i]);
  });

  // JSON stringify of a Map does not work, so we need to convert it to a object first.
  // TODO: Switch to Object.fromEnties when we bump target to es2019
  function toObject(map: Map<string, string>): object {
    const obj: any = { };
    for (const [k, v] of map) {
      obj[k] = v;
    }
    return obj;
  }
  return JSON.stringify(toObject(result));
}

export async function getElectionDetails(
  req: any, res: any, dummyuser: boolean = false,
): Promise<string> {
  let authError: string | null = null;
  let user: any = null;

  try {
    user = await getAuthenticatedUser(req, dummyuser);
  } catch (e) {
    if (e instanceof AuthenticationError) {
      // User is not authenticated, but that's OK, some elections are
      // visible to everyone.
      authError = e.toString();
    } else {
      throw e;
    }
  }
  const electionID = getGetParam<string>(req, 'election_id');
  const election = await getElection(electionID);
  if (election.visibility === VISIBILITY_PUBLIC) {
    return JSON.stringify(election);
  }
  if (user === null) {
    throw new AuthenticationError(
      `Election is not public and callee is not authenticated. Authentication error: ${authError}`,
    );
  }
  if (election.visibility !== VISIBILITY_PARTICIPANTS) {
    throw new ServerError('Election visiblity not implemented');
  }
  assert(user.user_id !== undefined);
  if (user.user_id !== election.adminAddress
    && !election.participantAddresses.includes(user.user_id)) {
    throw new NoAccessError('User does not have access to this election');
  }
  return JSON.stringify(election);
}

async function commonListElections(
  req: any, res: any, extraQueryConstraint: QueryCallback,
): Promise<[Array<Election>, string | null]> {
  const cursor = getGetParamOr<string | null>(req, 'cursor', null);
  const filterTagsIn = getGetParamOr<string | null>(req, 'filter_tags', null);
  const filterTags: string[] | null = filterTagsIn === null
    ? null
    : filterTagsIn.split(',');

  return listElections(
    DEFAULT_LIST_ELECTIONS_LIMIT,
    cursor, filterTags, extraQueryConstraint,
  );
}

export async function listPublicElections(
  req: any, res: any,
): Promise<string> {
  const [elections, nextCursor] = await commonListElections(req, res,
    (query) => query.where(
      constants.election.fields.visibility,
      '==', VISIBILITY_PUBLIC,
    ));

  return JSON.stringify({
    cursor: nextCursor,
    elections,
  });
}

export async function listHostedElections(
  req: any, res: any, dummyuser: boolean = false,
): Promise<string> {
  const userID = await getAuthenticatedUser(req, dummyuser);
  const [elections, nextCursor] = await commonListElections(req, res,
    (query) => query.where(
      constants.election.fields.adminAddress,
      '==', userID.user_id,
    ));

  return JSON.stringify({
    cursor: nextCursor,
    elections,
  });
}

export async function listParticipatingElections(
  req: any, res: any, dummyuser: boolean = false,
): Promise<string> {
  const userID = await getAuthenticatedUser(req, dummyuser);
  const filterTagsIn = getGetParamOr<string | null>(req, 'filter_tags', null);
  if (filterTagsIn !== null) {
    // Firestore does not support multiple 'array-contains' clauses,
    // and we need to use the one we have for filtering on participantAddresses (Sept 2021)
    throw new InvalidParamError('fiter_tags', 'Tags are not supported for this API call');
  }
  const [elections, nextCursor] = await commonListElections(req, res,
    (query) => query.where(
      constants.election.fields.participantAddresses,
      'array-contains', userID.user_id,
    ));

  return JSON.stringify({
    cursor: nextCursor,
    elections,
  });
}
