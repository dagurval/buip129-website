import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import { getAndroidNotificationTokens } from '../user/user_database';
import { toElection } from './election_database';

export default async function onNewElection(
  snapshot: functions.firestore.QueryDocumentSnapshot, context: functions.EventContext,
) {
  const election = toElection(snapshot);
  const { participantAddresses } = election;
  const androidNotificationTokens = await getAndroidNotificationTokens(participantAddresses);
  const pureAndroidNotificationTokens = androidNotificationTokens.filter(
    (token) => token != null,
  ) as string[];

  if (androidNotificationTokens.length === 0) {
    console.error(androidNotificationTokens);
    throw Error('androidNotificationTokens.length === 0');
  }
  try {
    const message: admin.messaging.MulticastMessage = {
      notification: {
        title: 'New election',
        body: election.description,
      },
      data: {
        pushType: 'on_createElection',
        election_id: election.id,
        election: JSON.stringify(election),
      },
      apns: {
        headers: {
          'apns-priority': '10',
        },
        payload: {
          aps: {
            'content-available': 1,
            sound: 'default',
          },
        },
      },
      tokens: pureAndroidNotificationTokens,
    };

    return admin.messaging().sendMulticast(message)
      .then((batchResponse) => {
        batchResponse.responses.forEach((response) => {
          if (response.error != null) {
            // ??
          }
        });
        return Promise.resolve();
      })
      .catch((error) => Promise.reject(error));
  } catch (error) {
    return Promise.reject(error);
  }
}
