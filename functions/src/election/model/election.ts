import admin from 'firebase-admin';

export const CONTRACT_TYPE_MULTI_OPTION_VOTE = 'multioptionvote';
export const CONTRACT_TYPE_RING_SIGNATURE = 'ringsignature';
export const CONTRACT_TYPE_TWO_OPTION_VOTE = 'twooptionvote';
export const VISIBILITY_PARTICIPANTS = 'participants';
export const VISIBILITY_PUBLIC = 'public';

export class Election {
  public id: string;

  public adminAddress: string;

  public description: string;

  public participantAddresses: string[] = [];

  public salt: string;

  public endHeight: number;

  public contractType: string;

  public visibility: string;

  public options: string[] = [];

  public tags: string[] = [];

  public createdAtTimestamp: admin.firestore.Timestamp;

  public createdAt: number;

  public beginHeight: number;

  constructor(
    id: string,
    adminAddress: string,
    description: string,
    participantAddresses: string[],
    salt: string,
    endHeight: number,
    contractType: string,
    visibility: string,
    options: string[],
    tags: string[],
    createdAtTimestamp: admin.firestore.Timestamp,
    createdAt: number,
    beginHeight: number,
  ) {
    this.id = id;
    this.adminAddress = adminAddress;
    this.description = description;
    this.participantAddresses = participantAddresses;
    this.salt = salt;
    this.endHeight = endHeight;
    this.contractType = contractType;
    this.visibility = visibility;
    this.options = options;
    this.tags = tags;
    this.createdAtTimestamp = createdAtTimestamp;
    this.createdAt = createdAt;
    this.beginHeight = beginHeight;
  }

  public encodeCreatedAt(): string {
    const buffer = Buffer.from(JSON.stringify([
      this.createdAtTimestamp.seconds, this.createdAtTimestamp.nanoseconds]), 'utf-8');
    return buffer.toString('base64');
  }

  static decodeCreatedAt(encodedTimestamp: string): admin.firestore.Timestamp {
    const buffer = Buffer.from(encodedTimestamp, 'base64');
    const json = buffer.toString('utf-8');
    const [seconds, nanoseconds] = JSON.parse(json);
    return new admin.firestore.Timestamp(seconds, nanoseconds);
  }
}
