/**
 * Error thrown when someone is attempting to access something they're not
 * have privilege to.
 */
export default class NoAccessError extends Error {

}
