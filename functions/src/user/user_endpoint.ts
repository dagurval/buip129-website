import { IAuthFirebase } from '../auth/model/auth';
import { verifySignature } from '../challenge';
import { constants } from '../constants';
import { firebaseAdminFirestore, logger } from '../db';
import {
  storeRingsigLink, getRingSignaturePubkey, getUser, getUserAllowedTags,
} from './user_database';
import {
  ServerError, InvalidParamError, SignatureError, UserError,
} from '../error';
import { getPostParam } from '../util';
import { getAuthenticatedUser } from '../rpcauth';

export async function storeChallenge(challenge: string, user: IAuthFirebase) {
    interface ILogin {
        readonly challenge: string;
    }
    const loginData: ILogin = {
      challenge,
    };

    await firebaseAdminFirestore().collection(constants.login.path)
      .doc(user.id)
      .set(loginData)
      .catch((error: any) => {
        console.warn('user', 'Store challenge error: %s', error);
        return Promise.reject(error);
      });
}

export async function getChallenge(userID: string): Promise<string> {
  const doc = await firebaseAdminFirestore().collection(constants.login.path)
    .doc(userID)
    .get();
  const challenge = doc.get('challenge');
  if (!challenge) {
    throw Error('Challenge not found');
  }
  return challenge;
}

export async function storeAuthToken(userid: string, token: string) {
  await firebaseAdminFirestore().collection(constants.login.path)
    .doc(userid)
    .set({ authtoken: token })
    .catch((error: any) => {
      console.warn('Store authtoken error: ', error);
      return Promise.reject(error);
    });
}

function isLCHexStr(str: string) {
  return str.match(/^[a-f0-9]+$/) !== null;
}

// eslint-disable-next-line
export enum LINK_PURPOSE {
    RingSignature = 'ringsignature',
}

const RINGSIG_PUBKEY_LEN = 32;

/**
 * Link a public key to a user.
 *
 * The user is telling us that he also owns a different public key that should
 * be associated with this account.
 *
 * We require a signed proof of this, so we can prove to other users this
 * ownership.
 *
 * Supports only linking ring signature public key, but extendable if needed.
 */
export async function linkPublicKey(req: any, _res: any): Promise<string> {
  const userID: string = getPostParam<string>(req, 'userid');
  const existingKeyPromise = getRingSignaturePubkey(userID);
  try {
    // check if user exists, this throws if not.
    await getUser(userID);
  } catch (e: any) {
    throw new InvalidParamError('userid', e.toString());
  }

  const purpose: string = getPostParam<string>(req, 'purpose');
  if (purpose !== LINK_PURPOSE.RingSignature) {
    throw new InvalidParamError('purpose', 'Unsupported purpose.');
  }

  const signature: string = getPostParam<string>(req, 'signature');
  const pubkey: string = getPostParam<string>(req, 'pubkey');
  if (!isLCHexStr(pubkey)) {
    throw new InvalidParamError('pubkey',
      'Must be lower-case hexadecimal string');
  }
  if (purpose === LINK_PURPOSE.RingSignature) {
    // * 2 because it's hex encoded
    if (pubkey.length !== (RINGSIG_PUBKEY_LEN * 2)) {
      const errmsg = "Invalid length for 'pubkey' parameter in combination "
                + `with purpose '${LINK_PURPOSE.RingSignature}'`;
      throw new InvalidParamError('pubkey', errmsg);
    }
  }

  // Check if user already has this link. It is not allowed to replace a link.
  const existingKey = await existingKeyPromise;
  if (existingKey === pubkey) {
    return JSON.stringify({
      status: `Linked ${pubkey}`,
    });
  }
  if (existingKey !== undefined) {
    logger().debug(`Existing key ${existingKey}, new key ${pubkey}`);
    throw new UserError(
      `User already has a different public key linked for purpose ${purpose}`,
    );
  }

  const message: string = `link:${pubkey}`;

  try {
    if (!verifySignature(message, userID, signature)) {
      throw new SignatureError('Signature verification failed');
    }
  } catch (e) {
    throw new SignatureError(`Signature verification error: ${e}`);
  }

  try {
    await storeRingsigLink(userID, pubkey, message, signature);
  } catch (e) {
    throw new ServerError(`Linking failed: ${e}`);
  }
  return JSON.stringify({
    status: `Linked ${pubkey}`,
  });
}

/**
 * Retrieve list of tags that user is allowed to assign to a new election.
 */
export async function listUserAllowedTags(req: any, res: any,
  dummyuser: boolean = false): Promise<string> {
  const user = await getAuthenticatedUser(req, dummyuser);
  return JSON.stringify({
    tags: await getUserAllowedTags(user.user_id),
  });
}
